Authors
=======

TimeTaco exists because of the work of these amazing people:

* Stavros Korokithakis (https://www.stavros.io)
* Stelios Petrakis (http://twitter.com/stelabouras)

If you've contributed some code, please add your name to the above list in your
merge request.
