terraform {
  backend "s3" {
    bucket = "terraformstate.stochastic.io"
    key    = "timetaco/terraform.tfstate"
    region = "eu-central-1"
  }
}

variable "cloudflare_email" {}
variable "cloudflare_token" {}

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

variable ipv6_ip { default = "2a01:4f8:1c0c:6109::1" }
variable ipv4_ip { default = "195.201.40.251" }
variable domain { default = "timetaco.com" }

resource "cloudflare_record" "root4" {
    domain = "${var.domain}"
    proxied = "true"
    type = "A"
    name = "@"
    value = "${var.ipv4_ip}"
}

resource "cloudflare_record" "www4" {
    domain = "${var.domain}"
    proxied = "true"
    type = "A"
    name = "www"
    value = "${var.ipv4_ip}"
}

resource "cloudflare_record" "root6" {
    domain = "${var.domain}"
    proxied = "true"
    type = "AAAA"
    name = "@"
    value = "${var.ipv6_ip}"
}

resource "cloudflare_record" "www6" {
    domain = "${var.domain}"
    proxied = "true"
    type = "AAAA"
    name = "www"
    value = "${var.ipv6_ip}"
}

resource "cloudflare_record" "bing" {
    domain = "${var.domain}"
    proxied = "false"
    type = "CNAME"
    name = "773ec9bd6c565b6304b81d89885e579a"
    value = "verify.bing.com"
}

resource "cloudflare_record" "email" {
    domain = "${var.domain}"
    proxied = "false"
    type = "CNAME"
    name = "email"
    value = "mailgun.org"
}

resource "cloudflare_record" "mx1" {
    domain = "${var.domain}"
    proxied = "false"
    type = "MX"
    name = "@"
    value = "mxa.mailgun.org"
    priority = "10"
}

resource "cloudflare_record" "mx2" {
    domain = "${var.domain}"
    proxied = "false"
    type = "MX"
    name = "@"
    value = "mxb.mailgun.org"
    priority = "20"
}

resource "cloudflare_record" "spf" {
    domain = "${var.domain}"
    proxied = "false"
    type = "TXT"
    name = "@"
    value = "v=spf1 include:mailgun.org ~all"
}

resource "cloudflare_record" "dkim" {
    domain = "${var.domain}"
    proxied = "false"
    type = "TXT"
    name = "k1._domainkey"
    value = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDE2ZipaO7oF0aBZrQqhYH9jYjtJg91hdO8FMm4DFS3vIK3s+tYBGRdVVbu5/div6a4A3fh8MPlW9QT5Xo+QRj2vDUWjUHbMEm6s49QL6xJlEtrElO/Scsvqa7lAuOE/rIfF0hHwbBBxKKa/1DD3OKxM4sN/Om6Zt2yAg4cFIqSVQIDAQAB"
}
