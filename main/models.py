from __future__ import unicode_literals

from datetime import datetime
from typing import Union

import requests
import shortuuid
from django.conf import settings
from django.contrib.auth import get_user_model, user_logged_in
from django.core.urlresolvers import reverse
from django.db import models
from django.db.utils import IntegrityError
from django.dispatch import receiver
from webpush import send_group_notification


def td_format(td_object):
    # From http://stackoverflow.com/a/13756038/28196.
    seconds = int(td_object.total_seconds())
    periods = [
        ("year", 60 * 60 * 24 * 365),
        ("month", 60 * 60 * 24 * 30),
        ("day", 60 * 60 * 24),
        ("hour", 60 * 60),
        ("minute", 60),
        ("second", 1),
    ]

    strings = []
    for period_name, period_seconds in periods:
        if seconds >= period_seconds:
            period_value, seconds = divmod(seconds, period_seconds)
            if period_value == 1:
                strings.append("%s %s" % (period_value, period_name))
            else:
                strings.append("%s %ss" % (period_value, period_name))

    return strings[0]


def uploaded_filename(instance, filename: str) -> str:
    if "." in filename:
        ext = filename.rsplit(".", 1)[-1].lower().strip()
    else:
        ext = "jpg"
    return "backgrounds/{}.{}".format(instance.pk, ext)


def generate_key() -> str:
    "Create an key."
    return shortuuid.ShortUUID().random(8)


def generate_uuid() -> str:
    "Create a UUID for an object."
    return shortuuid.ShortUUID("abdcefghjkmnpqrstuvwxyz").random()[:6]


class CountdownManager(models.Manager):
    def create(self, *args, **kwargs):
        """Create an object, retrying if there's an ID collision."""

        # Try to create new IDs for the object if one collides.
        tries = 10
        for x in range(tries):
            try:
                obj = super().create(*args, **kwargs)
            except IntegrityError:
                continue
            else:
                break
        else:
            raise IntegrityError("Could not find an ID after %s tries." % tries)

        return obj


class Countdown(models.Model):
    """The main countdown object."""

    # The countdown ID will also be its URL.
    id = models.CharField(max_length=100, primary_key=True, default=generate_uuid, editable=False)
    # The user that created this countdown.
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, blank=True, null=True)
    # The target date of the countdown.
    target = models.DateTimeField(db_index=True)
    # The title to be displayed on the countdown page.
    title = models.CharField(max_length=100, blank=True)
    # When the countdown was created.
    created = models.DateTimeField(auto_now_add=True)
    # The key the user can use to edit the countdown.
    key = models.CharField(max_length=64, default=generate_key)
    # The background image URL.
    background_url = models.CharField(max_length=10000, blank=True)
    # Whether the notification for this countdown has been sent.
    notification_sent = models.BooleanField(default=False, db_index=True)
    # Unsplash photo author name
    unsplash_author_name = models.CharField(max_length=10000, blank=True)
    # Unsplash photo author url
    unsplash_author_url = models.CharField(max_length=10000, blank=True)

    objects = CountdownManager()

    def __str__(self) -> str:
        return self.id

    def delete(self, *args, **kwargs):
        if self.background_url:
            version, url = self.background_url.rsplit("/")[-2:]
            name = url.split(".")[0]

            requests.delete(
                "https://api.cloudinary.com/v1_1/timetaco/resources/image/upload/",
                auth=settings.CLOUDINARY_CREDENTIALS,
                data={"public_ids": name},
            )
        super().delete(*args, **kwargs)

    @property
    def title_or_default(self) -> str:
        return self.title if self.title else "Taco"

    def get_og_image_url(self) -> Union[str, None]:
        """
        Return the URL for the resized background image, for the OpenGraph tag.
        """
        if settings.CLOUDINARY_IMAGES and self.background_url:
            version, url = self.background_url.rsplit("/")[-2:]

            return (
                "https://res.cloudinary.com/timetaco/image/upload/c_fill,h_630,w_1200,f_auto/l_one_pixel,w_1200,h_630,e_colorize,co_rgb:EEE36D,o_50/w_1000,c_fit,l_text:Helvetica_100_bold:%s%%0A%s,co_rgb:6C59A3/%s/%s"
                % (self.title_or_default, self.target, version, url)
            )

        return None

    def get_background_url(self) -> Union[str, None]:
        """
        Return the URL for the background image.
        Doesn't allow the generated image to be larger than 1000x1000.
        """
        if settings.CLOUDINARY_IMAGES and self.background_url:
            version, url = self.background_url.rsplit("/")[-2:]

            return "https://res.cloudinary.com/timetaco/image/upload/w_1000,h_1000,c_limit,f_auto/%s/%s" % (
                version,
                url,
            )

        return None

    def get_absolute_url(self) -> str:
        return reverse("main:countdown-view", args=[self.id])

    def time_to_target(self) -> str:
        """
        Return a string that contains the approximate time to target.

        Example: "has already expired", "expires in 1 day", "expires in 2 hours".
        """
        delta = self.target - datetime.utcnow()
        if delta.total_seconds() < 0:
            return "has ended"
        else:
            return "ends in %s" % td_format(delta)
        return delta

    def send_notification(self):
        """
        Send a notification to subscribed users.
        """
        payload = {
            "head": "TimeTaco notification",
            "body": "%s %s!" % (self.title_or_default, self.time_to_target()),
            "id": self.id,
        }

        try:
            send_group_notification(group_name=self.id, payload=payload, ttl=30 * 60)
        except:  # noqa
            # Ignore whatever goes wrong here.
            pass

        self.notification_sent = True
        self.save()


@receiver(user_logged_in)
def user_login(sender, request, user, **kwargs):
    # Associate a user's anonymous countdowns with their account.
    user_countdowns = Countdown.objects.filter(id__in=request.session.get("tacos", []))
    for countdown in user_countdowns:
        countdown.user = user
        countdown.save()
