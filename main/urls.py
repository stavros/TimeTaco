from django.conf import settings
from django.conf.urls import url
from django.views.static import serve
from main import views

urlpatterns = [
    url(r"^$", views.home, name="home"),
    url(r"^about/$", views.about, name="about"),
    url(r"^account/$", views.account, name="account"),
    url(r"^update/$", views.update_countdown, name="countdown-update"),
    url(r"^webhook/notifications/$", views.webhook_notifications, name="webhook-notifications"),
    url(r"^(?P<countdown_id>[^/]{6})/$", views.view_countdown, name="countdown-view"),
    url(r"^(?P<countdown_id>[^/]{6})/embed/$", views.view_embed, name="embed-view"),
    url(r"^(?P<countdown_id>[^/]{6})/delete/$", views.delete_countdown, name="delete-countdown"),
]

if settings.DEBUG:
    urlpatterns += [url(r"^media/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT, "show_indexes": True})]
