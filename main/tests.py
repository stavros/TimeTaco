import os
from datetime import datetime, timedelta

import hypothesis.strategies as st
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.template import Template
from django.test import TestCase
from hypothesis.extra.django.models import default_value, models

from .models import Countdown

User = get_user_model()

CountdownFactory = models(
    Countdown,
    id=default_value,
    target=st.integers(min_value=0, max_value=1000000).map(lambda x: datetime.now() + timedelta(minutes=x)),
    created=st.just(datetime.now()),
    title=st.text(max_size=100),
    background_url=st.just("https://example.com/image.jpg"),
)


class SmokeTests(TestCase):
    def setUp(self):
        self.countdown1 = CountdownFactory.example()
        self.countdown2 = CountdownFactory.example()
        self.user1 = User.objects.create_user("user1", "user1@example.com", "password")

    def test_compile_templates(self):
        for template_dir in settings.TEMPLATES[0]["DIRS"]:
            for basepath, dirs, filenames in os.walk(template_dir):
                for filename in filenames:
                    path = os.path.join(basepath, filename)
                    with open(path, "r") as f:
                        # This will fail if the template cannot compile
                        t = Template(f.read())
                        assert t

    def test_urls(self):
        response = self.client.get(reverse("main:home"))
        self.assertEqual(response.status_code, 200)

    def test_creating(self):
        # Create countdown.
        response = self.client.get(reverse("main:home"))
        form = response.context["form"]
        data = form.initial
        data["target"] = "2100-10-2 02:03:04"
        data["title"] = "Hello!"

        response = self.client.post(reverse("main:countdown-update"), data, follow=True)
        countdown_id = response.context["countdown"].id
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Hello!", response.content)

        response = self.client.get(reverse("main:countdown-view", args=[countdown_id]))
        self.assertEqual(response.status_code, 200)

        # Create another countdown.
        response = self.client.get(reverse("main:home"))
        form = response.context["form"]
        data = form.initial
        data["target"] = "2100-10-2 02:03:04"
        data["title"] = "Hello!"

        response = self.client.post(reverse("main:countdown-update"), data, follow=True)
        countdown_id = response.context["countdown"].id
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Hello!", response.content)

        response = self.client.get(reverse("main:countdown-view", args=[countdown_id]))
        self.assertEqual(response.status_code, 200)

        # Log the user in to check if the session countdowns were associated successfully.
        self.client.login(username=self.user1.username, password="password")

        self.user1.refresh_from_db()
        self.assertEqual(self.user1.countdown_set.count(), 2)

        # Create another countdown.
        response = self.client.get(reverse("main:home"))
        form = response.context["form"]
        data = form.initial
        data["target"] = "2100-10-2 02:03:04"
        data["title"] = "Hello!"

        response = self.client.post(reverse("main:countdown-update"), data, follow=True)
        countdown_id = response.context["countdown"].id
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Hello!", response.content)

        response = self.client.get(reverse("main:countdown-view", args=[countdown_id]))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(self.user1.countdown_set.count(), 3)

        Countdown.objects.create(target=datetime(3000, 1, 1, 0, 0, 0), title="Hi")

        self.assertEqual(self.user1.countdown_set.count(), 3)


class UnitTests(TestCase):
    def test_time_to_target(self):
        def fdate(days, seconds):
            return datetime.utcnow() + timedelta(days=days, seconds=seconds)

        self.assertEqual(Countdown(target=fdate(10, 100)).time_to_target(), "ends in 10 days")
        self.assertEqual(Countdown(target=fdate(-1, 100)).time_to_target(), "has ended")
        self.assertEqual(Countdown(target=fdate(1, 100)).time_to_target(), "ends in 1 day")
        self.assertEqual(Countdown(target=fdate(0, 7100)).time_to_target(), "ends in 1 hour")
        self.assertEqual(Countdown(target=fdate(4, 23 * 3600)).time_to_target(), "ends in 4 days")
        self.assertEqual(Countdown(target=fdate(0, 100)).time_to_target(), "ends in 1 minute")
        self.assertEqual(Countdown(target=fdate(0, 130)).time_to_target(), "ends in 2 minutes")
