import datetime

from annoying.decorators import render_to
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.utils.crypto import constant_time_compare
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.http import require_http_methods

from .forms import CountdownForm
from .models import Countdown


@render_to("home.html")
def home(request):
    "Render the home page."

    is_producthunt = request.GET.get("ref") == "producthunt"

    return {"form": CountdownForm(), "producthunt": is_producthunt}


@login_required()
@render_to("account.html")
def account(request):
    "Render the account page."

    countdowns = Countdown.objects.filter(user=request.user)

    return {"countdowns": countdowns}


@render_to("about.html")
def about(request):
    "Render the about page."

    return {}


@login_required()
def delete_countdown(request, countdown_id):
    countdown = get_object_or_404(Countdown, pk=countdown_id, user=request.user)
    countdown.delete()
    messages.success(request, "Your taco has been deleted.")
    return redirect("main:account")


def webhook_notifications(request):
    # Get the current UTC date and look 30 minutes in the future
    ending_date = datetime.datetime.utcnow() + datetime.timedelta(seconds=settings.EXPIRING_COUNTER_INTERVAL)
    countdowns = Countdown.objects.filter(target__lte=ending_date).exclude(notification_sent=True)

    for countdown in countdowns:
        countdown.send_notification()

    return HttpResponse("OK")


@require_http_methods(["POST"])
def update_countdown(request):
    "Create or alter a countdown."
    if request.GET.get("id"):
        countdown = Countdown.objects.filter(pk=request.GET["id"]).first()
        if not countdown:
            messages.error(request, "No such countdown exists.")
            return redirect("main:home")
        if not constant_time_compare(request.GET.get("key", ""), countdown.key):
            messages.error(request, "The update key was invalid.")
            return redirect("main:home")
        form = CountdownForm(request.POST, request.FILES, instance=countdown, initial={"user": request.user})
    else:
        form = CountdownForm(request.POST, request.FILES, initial={"user": request.user})

    new_countdown = form.save(commit=False)
    new_countdown.notification_sent = False
    if request.user.is_authenticated:
        new_countdown.user = request.user
    new_countdown.save()

    if request.GET.get("id"):
        messages.success(request, "Your countdown has been created!")
    else:
        messages.success(request, "Your countdown has been updated!")

    if request.user.is_anonymous:
        # If the user is anonymous, store their created countdowns in their session.
        request.session["tacos"] = request.session.get("tacos", []) + [new_countdown.id]

    return redirect(new_countdown)


@render_to("view_countdown.html")
def view_countdown(request, countdown_id):
    "Show a countdown."
    countdown = get_object_or_404(Countdown, pk=countdown_id)

    show_edit = constant_time_compare(request.GET.get("key", ""), countdown.key)
    is_owner = countdown.user == request.user or countdown_id in request.session.get("tacos", []) or show_edit

    if show_edit:
        return TemplateResponse(request, "home.html", {"countdown": countdown})

    webpush = {"group": countdown.id}

    return {
        "countdown": countdown,
        "is_owner": is_owner,
        "webpush": webpush,
        "vapid_public_key": getattr(settings, "WEBPUSH_SETTINGS", {}).get("VAPID_PUBLIC_KEY", ""),
    }


@xframe_options_exempt
@render_to("embed.html")
def view_embed(request, countdown_id):
    countdown = get_object_or_404(Countdown, pk=countdown_id)

    return {"countdown": countdown, "host": request.GET.get("host", "")}
