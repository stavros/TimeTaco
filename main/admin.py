from django.contrib import admin
from djangoql.admin import DjangoQLSearchMixin

from .models import Countdown


@admin.register(Countdown)
class CountdownAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = ["id", "title", "target", "background_url", "created", "user"]
    search_fields = ["id", "title"]
    list_filter = ("created", "target")
    ordering = ["-created"]
