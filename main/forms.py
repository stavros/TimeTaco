from django import forms

from .models import Countdown


class CountdownForm(forms.ModelForm):
    class Meta:
        model = Countdown
        fields = ["target", "title", "background_url", "unsplash_author_name", "unsplash_author_url"]
