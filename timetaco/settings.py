import os
import re
from subprocess import check_output  # noqa
from typing import Dict, Union

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY", "secret")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True if os.environ.get("NODEBUG") is None else False

ALLOWED_HOSTS = ["localhost", "web"] if os.environ.get("NODEBUG") is None else [".timetaco.com"]

DEFAULT_FROM_EMAIL = "Timetaco <support@timetaco.com>"

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"


# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django.contrib.sessions",
    "django.contrib.messages",
    "whitenoise.runserver_nostatic",
    "django.contrib.staticfiles",
    "django_extensions",
    "django_nose",
    "djangoql",
    "storages",
    "main",
    "webpush",
    "tokenauth",
    "raven.contrib.django.raven_compat",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "timetaco.stats_middleware.StatsMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

AUTHENTICATION_BACKENDS = ("tokenauth.auth_backends.EmailTokenBackend", "django.contrib.auth.backends.ModelBackend")

TEST_RUNNER = "django_nose.NoseTestSuiteRunner"

NOSE_ARGS = ["--nocapture", "--nologcapture", "--stop"]
NOSE_ARGS += [
    "--cover-package=timetaco",
    "--cover-package=main",
    "--cover-erase",
    "--cover-html",
    "--cover-html-dir=htmlcov",
]
NOSE_ARGS += ["--with-coverage"]

ROOT_URLCONF = "timetaco.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "timetaco.context_processors.settings",
            ]
        },
    }
]

WSGI_APPLICATION = "timetaco.wsgi.application"


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

if os.environ.get("IN_DOCKER"):
    DEFAULT_FILE_STORAGE = "storages.backends.s3boto.S3BotoStorage"

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": os.environ.get("DB_NAME", "timetaco"),
            "USER": os.environ.get("DB_USER", "timetaco"),
            "PASSWORD": os.environ.get("DB_PASSWORD", ""),
            "HOST": os.environ.get("DB_HOST", ""),
            "PORT": os.environ.get("DB_PORT", 5432),
        }
    }
elif os.environ.get("DATABASE_URL"):
    # Stuff for when running in Dokku.

    # Parse the DATABASE_URL env var.
    USER, PASSWORD, HOST, PORT, NAME = re.match(  # type: ignore
        r"^postgres://(?P<username>.*?)\:(?P<password>.*?)\@(?P<host>.*?)\:(?P<port>\d+)\/(?P<db>.*?)$",
        os.environ.get("DATABASE_URL", ""),
    ).groups()

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": NAME,
            "USER": USER,
            "PASSWORD": PASSWORD,
            "HOST": HOST,
            "PORT": int(PORT),
        }
    }
    DEFAULT_FILE_STORAGE = "storages.backends.s3boto.S3BotoStorage"

    EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"

    SENDGRID_API_KEY = os.getenv("EMAIL_HOST_PASSWORD", "pass")
    SENDGRID_SANDBOX_MODE_IN_DEBUG = DEBUG
else:
    DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": os.path.join(BASE_DIR, "db.sqlite3")}}


# AWS details, for production.
AWS_ACCESS_KEY_ID = os.environ.get("AWS_KEY_ID", "")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_KEY", "")
AWS_STORAGE_BUCKET_NAME = "media.timetaco.com"
AWS_QUERYSTRING_AUTH = False
AWS_S3_CALLING_FORMAT = "boto.s3.connection.OrdinaryCallingFormat"
AWS_S3_HOST = "s3-us-east-2.amazonaws.com"

# Unsplash settings.
UNSPLASH_KEY = os.environ.get("UNSPLASH_KEY", "")

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]


try:
    COMMIT_HASH = check_output(["git", "rev-parse", "--short", "HEAD"])
except:  # noqa
    COMMIT_HASH = "Not a git repo"


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = False

USE_L10N = False

USE_TZ = False

LOGIN_REDIRECT_URL = "/"
LOGIN_URL = "/"
LOGOUT_REDIRECT_URL = LOGIN_REDIRECT_URL

SITE_ID = 1

CLOUDINARY_IMAGES = False

CLOUDINARY_CREDENTIALS = (
    os.environ.get("CLOUDINARY_USERNAME", "881365652686615"),
    os.environ.get("CLOUDINARY_PASSWORD", "find me"),
)

EXPIRING_COUNTER_INTERVAL = 30 * 60  # 30 minutes in seconds

WEBPUSH_SETTINGS = {
    "VAPID_PUBLIC_KEY": os.environ.get("VAPID_PUBLIC_KEY", "timetaco"),
    "VAPID_PRIVATE_KEY": os.environ.get("VAPID_PRIVATE_KEY", "timetaco"),
    "VAPID_ADMIN_EMAIL": "support@timetaco.com",
}

RAVEN_CONFIG: Dict[str, Union[None, str]] = {"dsn": os.environ.get("RAVEN_DSN")}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "_static")
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

STATICFILES_DIRS = [os.path.join(BASE_DIR, "static")]

try:
    from .local_settings import *  # noqa
except ImportError:
    pass
