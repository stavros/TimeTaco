from django.conf import settings as setting_dict


def settings(request):
    return {"settings": setting_dict}
