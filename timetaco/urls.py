from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r"^auth/", include("tokenauth.urls", namespace="tokenauth")),
    url(r"^entrary/", admin.site.urls),
    url(r"^", include("main.urls", namespace="main")),
    url(r"^webpush/", include("webpush.urls")),
]
