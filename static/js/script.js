(function() {

	var ConfirmAction = {

	  initialize : function() {

	  	var confirmations = Array.from(document.querySelectorAll('[data-confirm]'));

	    if(confirmations.length == 0)
	      return;

	  	for(confirmationIndex in confirmations) {

	  		var confirmation = confirmations[confirmationIndex];

	  		confirmation.addEventListener('submit', function(event) {

	  			event.preventDefault();

	  			var question = this.dataset.confirm;

	  			var confirmed = confirm(question);

	  			if(confirmed)
	  				this.submit();

	  			return false

	  		}, false);
	  	}
	  }
	}

	window.ConfirmAction = ConfirmAction;
})();

(function() {

	var AccountDetails = {

		initialize : function() {

			if(document.querySelector('body.account-page') == null)
				return;

			var gravatarElement = document.querySelector('[data-gravatar]');
			var email = gravatarElement.dataset.gravatar;
			var gravatarImageURL = this.gravatarImageURL(email, {
				'backup' : 'https://www.timetaco.com/static/css/default-avatar.png'
			});

			gravatarElement.style.backgroundImage = 'url(\'' + gravatarImageURL + '\')';

			var tacoList = Array.from(document.querySelectorAll('.taco-list li'));

			for(tacoIndex in tacoList) {

				var taco = tacoList[tacoIndex];

				var iconElement = taco.querySelector('.icon');
				var iconURL = iconElement.dataset.icon;

				if(iconURL.length > 0) {

					ColorExtractor.extractColorFromImage(iconURL, (rgbColor, imageURL) => {

						var iconElement = document.querySelector('[data-icon="' + imageURL + '"]');
						var size = iconElement.offsetHeight * 2;
						var icon = ColorExtractor.canvasWithColor(rgbColor, size).toDataURL('image/png');

						iconElement.style.backgroundImage = 'url(\'' + icon + '\')';
					});
				}
				else {

					var size = iconElement.offsetHeight * 2;
					var defaultIcon = ColorExtractor.defaultTacoIconCanvas(size).toDataURL('image/png');

					iconElement.style.backgroundImage = 'url(\'' + defaultIcon + '\')';
				}
			}
		},

		gravatarImageURL : function(email, options) {

			// using md5() from here: http://www.myersdaily.org/joseph/javascript/md5-text.html
			function md5cycle(e,t){var n=e[0],r=e[1],i=e[2],s=e[3];n=ff(n,r,i,s,t[0],7,-680876936);s=ff(s,n,r,i,t[1],12,-389564586);i=ff(i,s,n,r,t[2],17,606105819);r=ff(r,i,s,n,t[3],22,-1044525330);n=ff(n,r,i,s,t[4],7,-176418897);s=ff(s,n,r,i,t[5],12,1200080426);i=ff(i,s,n,r,t[6],17,-1473231341);r=ff(r,i,s,n,t[7],22,-45705983);n=ff(n,r,i,s,t[8],7,1770035416);s=ff(s,n,r,i,t[9],12,-1958414417);i=ff(i,s,n,r,t[10],17,-42063);r=ff(r,i,s,n,t[11],22,-1990404162);n=ff(n,r,i,s,t[12],7,1804603682);s=ff(s,n,r,i,t[13],12,-40341101);i=ff(i,s,n,r,t[14],17,-1502002290);r=ff(r,i,s,n,t[15],22,1236535329);n=gg(n,r,i,s,t[1],5,-165796510);s=gg(s,n,r,i,t[6],9,-1069501632);i=gg(i,s,n,r,t[11],14,643717713);r=gg(r,i,s,n,t[0],20,-373897302);n=gg(n,r,i,s,t[5],5,-701558691);s=gg(s,n,r,i,t[10],9,38016083);i=gg(i,s,n,r,t[15],14,-660478335);r=gg(r,i,s,n,t[4],20,-405537848);n=gg(n,r,i,s,t[9],5,568446438);s=gg(s,n,r,i,t[14],9,-1019803690);i=gg(i,s,n,r,t[3],14,-187363961);r=gg(r,i,s,n,t[8],20,1163531501);n=gg(n,r,i,s,t[13],5,-1444681467);s=gg(s,n,r,i,t[2],9,-51403784);i=gg(i,s,n,r,t[7],14,1735328473);r=gg(r,i,s,n,t[12],20,-1926607734);n=hh(n,r,i,s,t[5],4,-378558);s=hh(s,n,r,i,t[8],11,-2022574463);i=hh(i,s,n,r,t[11],16,1839030562);r=hh(r,i,s,n,t[14],23,-35309556);n=hh(n,r,i,s,t[1],4,-1530992060);s=hh(s,n,r,i,t[4],11,1272893353);i=hh(i,s,n,r,t[7],16,-155497632);r=hh(r,i,s,n,t[10],23,-1094730640);n=hh(n,r,i,s,t[13],4,681279174);s=hh(s,n,r,i,t[0],11,-358537222);i=hh(i,s,n,r,t[3],16,-722521979);r=hh(r,i,s,n,t[6],23,76029189);n=hh(n,r,i,s,t[9],4,-640364487);s=hh(s,n,r,i,t[12],11,-421815835);i=hh(i,s,n,r,t[15],16,530742520);r=hh(r,i,s,n,t[2],23,-995338651);n=ii(n,r,i,s,t[0],6,-198630844);s=ii(s,n,r,i,t[7],10,1126891415);i=ii(i,s,n,r,t[14],15,-1416354905);r=ii(r,i,s,n,t[5],21,-57434055);n=ii(n,r,i,s,t[12],6,1700485571);s=ii(s,n,r,i,t[3],10,-1894986606);i=ii(i,s,n,r,t[10],15,-1051523);r=ii(r,i,s,n,t[1],21,-2054922799);n=ii(n,r,i,s,t[8],6,1873313359);s=ii(s,n,r,i,t[15],10,-30611744);i=ii(i,s,n,r,t[6],15,-1560198380);r=ii(r,i,s,n,t[13],21,1309151649);n=ii(n,r,i,s,t[4],6,-145523070);s=ii(s,n,r,i,t[11],10,-1120210379);i=ii(i,s,n,r,t[2],15,718787259);r=ii(r,i,s,n,t[9],21,-343485551);e[0]=add32(n,e[0]);e[1]=add32(r,e[1]);e[2]=add32(i,e[2]);e[3]=add32(s,e[3])}function cmn(e,t,n,r,i,s){t=add32(add32(t,e),add32(r,s));return add32(t<<i|t>>>32-i,n)}function ff(e,t,n,r,i,s,o){return cmn(t&n|~t&r,e,t,i,s,o)}function gg(e,t,n,r,i,s,o){return cmn(t&r|n&~r,e,t,i,s,o)}function hh(e,t,n,r,i,s,o){return cmn(t^n^r,e,t,i,s,o)}function ii(e,t,n,r,i,s,o){return cmn(n^(t|~r),e,t,i,s,o)}function md51(e){txt="";var t=e.length,n=[1732584193,-271733879,-1732584194,271733878],r;for(r=64;r<=e.length;r+=64){md5cycle(n,md5blk(e.substring(r-64,r)))}e=e.substring(r-64);var i=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];for(r=0;r<e.length;r++)i[r>>2]|=e.charCodeAt(r)<<(r%4<<3);i[r>>2]|=128<<(r%4<<3);if(r>55){md5cycle(n,i);for(r=0;r<16;r++)i[r]=0}i[14]=t*8;md5cycle(n,i);return n}function md5blk(e){var t=[],n;for(n=0;n<64;n+=4){t[n>>2]=e.charCodeAt(n)+(e.charCodeAt(n+1)<<8)+(e.charCodeAt(n+2)<<16)+(e.charCodeAt(n+3)<<24)}return t}function rhex(e){var t="",n=0;for(;n<4;n++)t+=hex_chr[e>>n*8+4&15]+hex_chr[e>>n*8&15];return t}function hex(e){for(var t=0;t<e.length;t++)e[t]=rhex(e[t]);return e.join("")}function md5(e){return hex(md51(e))}function add32(e,t){return e+t&4294967295}var hex_chr="0123456789abcdef".split("");if(md5("hello")!="5d41402abc4b2a76b9719d911017c592"){function add32(e,t){var n=(e&65535)+(t&65535),r=(e>>16)+(t>>16)+(n>>16);return r<<16|n&65535}}
			//check to make sure you gave us something
			var options = options || {},
				base,
				params = [];

			//set some defaults, just in case
			options = {
				size: options.size || "120",
				rating: options.rating || "g",
				secure: options.secure || (location.protocol === 'https:'),
				backup: options.backup || ""
			};

			//setup the email address
			email = email.trim().toLowerCase();

			//determine which base to use
			base = options.secure ? 'https://secure.gravatar.com/avatar/' : 'http://www.gravatar.com/avatar/';

			//add the params
			if (options.rating) {params.push("r=" + options.rating)};
			if (options.backup) {params.push("d=" + encodeURIComponent(options.backup))};
			if (options.size) {params.push("s=" + options.size)};

			//now throw it all together
			return base + md5(email) + "?" + params.join("&");
		}
	}

	window.AccountDetails = AccountDetails;
})();

(function() {

	var ColorExtractor = {

		colorThief : undefined,

		initialize : function() {

			if(typeof ColorThief == 'undefined')
				return;

			this.colorThief = new ColorThief();

			this.parseBackgroundImage();
		},

		parseBackgroundImage : function() {

			if(typeof this.colorThief == 'undefined')
				return;

			var backgroundImage = document.body.style.backgroundImage.slice(4, -1).replace(/"/g, "");

			if(typeof backgroundImage == 'undefined' || backgroundImage.length == 0) {

				this.updateBackgroundWithColor(undefined);
				this.updateFaviconWithColor(undefined);
				return;
			}

			// Convert original uploaded image to a thumbnail in
			// order to download a smaller image
			//
			// E.g. from:
			// https://res.cloudinary.com/timetaco/image/upload/v1526205018/zxyw3kkvnoyol858zpxe.png
			// to:
			// https://res.cloudinary.com/timetaco/image/upload/w_100/zxyw3kkvnoyol858zpxe.png
			//
			// ref:
			// https://cloudinary.com/documentation/image_transformations#resizing_and_cropping_images
			var thumbImage = backgroundImage.replace(/\/v[0-9]+\//g,"/w_100/");

			this.extractColorFromImage(thumbImage, (rgbColor, imageURL) => {

				this.updateBackgroundWithColor(rgbColor);
				this.updateFaviconWithColor(rgbColor);
			});
		},

		extractColorFromImage : function(imageURL, completion) {

			if(typeof imageURL == 'undefined' || imageURL.length == 0) {

				completion(undefined, imageURL);
				return;
			}

			var downloadImage = new Image();
			downloadImage.crossOrigin = "Anonymous";
            downloadImage.onload = () => {

				var color = this.colorThief.getColor(downloadImage);
				var rgbColor = undefined;

				if(color.length == 3)
					rgbColor = 'rgb('+color[0]+','+color[1]+','+color[2]+')';

				completion(rgbColor, imageURL);
            }
            downloadImage.src = imageURL;
		},

		updateBackgroundWithColor: function(rgbColor) {

			if(typeof rgbColor == 'undefined')
				rgbColor = '#eee36d';

			if(document.body.className == 'pattern')
				return;

			document.body.style.backgroundColor = rgbColor;
			document.documentElement.style.backgroundColor = rgbColor;
		},

		updateFaviconWithColor: function(rgbColor) {

			// Remove existing favicon
			var element = document.querySelector('[type="image/x-icon"]');

			if(typeof element != 'undefined')
				element.parentNode.removeChild(element);

			var faviconURL = this.defaultFaviconURL();

			if(typeof rgbColor != 'undefined')
				faviconURL = this.faviconURLWithColor(rgbColor);

			// Set the generated favicon as the current one
			var link = document.createElement('link');
			link.type = 'image/x-icon';
			link.rel = 'shortcut icon';
			link.href = faviconURL;
			document.getElementsByTagName('head')[0].appendChild(link);
		},

		defaultTacoIconCanvas : function(size) {

			return this.canvasWithColors('#6C59A3', '#eee36d', size);
		},

		canvasWithColors: function(backgroundColor, foregroundColor, size) {

			var canvas = document.createElement('canvas');
			canvas.width = size;
			canvas.height = size;

			var ctx = canvas.getContext('2d');
			ctx.beginPath();
			ctx.fillStyle = backgroundColor;
			ctx.arc(size / 2, size / 2, size / 2,0,2*Math.PI);
			ctx.fill();
			ctx.beginPath();
			ctx.fillStyle = foregroundColor;
			ctx.arc(size / 2, size / 2, size / 4,0,2*Math.PI);
			ctx.fill();

			return canvas;
		},

		canvasWithColor: function(rgbColor, size) {

			var isDark = this.isDark(rgbColor);

			var shadedColor = this.shadeRGBColor(rgbColor, (isDark ? 0.25 : -0.25));

			var backgroundColor = (isDark ? shadedColor : rgbColor);
			var foregroundColor = (isDark ? rgbColor : shadedColor);

			return this.canvasWithColors(backgroundColor, foregroundColor, size);
		},

		defaultFaviconURL : function() {

			return this.defaultTacoIconCanvas(32).toDataURL("image/x-icon");
		},

		faviconURLWithColor : function(rgbColor) {

			return this.canvasWithColor(rgbColor, 32).toDataURL("image/x-icon");
		},

		shadeRGBColor : function(color, percent) {

		    var f=color.split(","),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);
		    return "rgb("+(Math.round((t-R)*p)+R)+","+(Math.round((t-G)*p)+G)+","+(Math.round((t-B)*p)+B)+")";
		},

		isDark : function(color) {

		    var f=color.split(","),R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);

		    var brightness = (R * 299 + G * 587 + B * 114) / 1000;

		    return brightness < 128;
		}
	}

	window.ColorExtractor = ColorExtractor;
})();

(function() {

  var Tooltips = {

  	tooltips : null,

    initialize : function() {

		this.tooltips = document.querySelectorAll('.tooltip');

		var self = this;

    	for(var i = 0; i < this.tooltips.length; i++) {

    		var tooltip = this.tooltips[i];

			tooltip.addEventListener('touchstart', function() {

    			self.disableActiveTooltips(this);
    		});

			tooltip.addEventListener('mouseover', function() {

    			self.disableActiveTooltips(this);
    		});

			tooltip.addEventListener('click', function() {

				self.disableActiveTooltips();
			});
    	}
    },

    disableActiveTooltips : function(activeTooltip) {

		this.tooltips = document.querySelectorAll('.tooltip');

    	for(var i = 0; i < this.tooltips.length; i++) {

    		var tooltip = this.tooltips[i];

    		if(typeof activeTooltip != 'undefined' &&
    			tooltip == activeTooltip)
    			continue;

			tooltip.dataset.tooltipactive = 0;
    	}
    }
  };

  window.Tooltips = Tooltips;

})();


(function() {

  var WebPush = {

    isPushEnabled: false,
    pushButton: null,
    webPushDataElement: null,
    registration: null,
    tacoId : null,

    initialize : function() {

		// Check if push messaging is supported
		if (!('PushManager' in window))
			return;

		// Check if browser Supports Service Workers
		if (!('serviceWorker' in navigator))
			return;

		// Check the current Notification permission.
		// If its denied, it's a permanent block until the
		// user changes the permission
		if (Notification.permission === 'denied')
			return;

		if(TimeTacoMeter.date == undefined)
			return;

		if(typeof expiring_counter_interval == 'undefined')
			return;

		var currentDate = new Date();

		var timeDiff = TimeTacoMeter.date - currentDate;
		var diffSecs = Math.ceil(timeDiff / 1000);

		if(diffSecs <= expiring_counter_interval)
			return;

		this.pushButton = document.getElementById('subscribe-button');

		if(!this.pushButton)
			return;

		if(localStorage.savedTacos)
			this.savedTacos = JSON.parse(localStorage.savedTacos);
		else
			this.savedTacos = [];

		var serviceWorker = document.getElementById('service-worker-js').src;

		navigator.serviceWorker.register(serviceWorker)
		.then(function(reg) {

			// Are Notifications supported in the service worker?
			if (!(reg.showNotification))
				return;

			this.registration = reg;
			this.webPushDataElement = document.getElementById('webpush-data');
			this.tacoId = this.webPushDataElement.dataset.group;

			this.pushButton.style.display = 'inline-block';

			if(this.doesTacoExistInList()) {

				this.pushButton.dataset.active = 1;
				this.isPushEnabled = true;
				this.pushButton.querySelector('.tooltiptext').innerText = "Disable notification for this taco";
			}
			else {
				this.pushButton.dataset.active = 0;
				this.pushButton.dataset.tooltipactive = 1;
			}

			this.pushButton.addEventListener('click', function() {

				if (this.isPushEnabled)
					return this.unsubscribe();

				this.subscribe(this.registration);

			}.bind(this));

		}.bind(this));
    },

    addTacoToList : function() {

    	if(this.doesTacoExistInList())
    		return;

    	this.savedTacos.push(this.tacoId);

    	localStorage.savedTacos = JSON.stringify(this.savedTacos);
    },

    removeTacoFromList : function() {

    	if(!this.doesTacoExistInList())
    		return;

		var tacoIndex = this.savedTacos.indexOf(this.tacoId);

		this.savedTacos.splice(tacoIndex, 1);

    	localStorage.savedTacos = JSON.stringify(this.savedTacos);
    },

    doesTacoExistInList : function() {

    	if(this.tacoId == undefined)
    		return false;

    	if(this.savedTacos.length == 0)
    		return false;

		return (this.savedTacos.indexOf(this.tacoId) > -1);
    },

	urlB64ToUint8Array : function(base64String) {

		const padding = '='.repeat((4 - base64String.length % 4) % 4);
		const base64 = (base64String + padding)
			.replace(/\-/g, '+')
			.replace(/_/g, '/');

		const rawData = window.atob(base64);
		const outputArray = new Uint8Array(rawData.length);

		for (var i = 0; i < rawData.length; ++i)
			outputArray[i] = rawData.charCodeAt(i);

		return outputArray;
	},

    getSubscription : function(reg) {

		return reg.pushManager.getSubscription()
		.then(function(subscription) {

		    // Check if Subscription is available
		    if(subscription)
				return subscription;

		    // If not, register one
			var metaObj = document.querySelector('meta[name="django-webpush-vapid-key"]');
			var applicationServerKey = metaObj.content;
			var options = {
				userVisibleOnly: true
			};

			if (applicationServerKey)
				options.applicationServerKey = this.urlB64ToUint8Array(applicationServerKey)

			return this.registration.pushManager.subscribe(options)

		}.bind(this))
    },

    subscribe : function(reg) {

		this.getSubscription(reg)
        .then(function(subscription) {

        	this.addTacoToList();
        	this.postSubscribeObj('subscribe', subscription);

        }.bind(this))
        .catch(function(error) {
        	alert(error);
        });
    },

    unsubscribe : function() {

		this.getSubscription(this.registration)
		.then(function(subscription) {

        	this.removeTacoFromList();
            this.postSubscribeObj('unsubscribe', subscription);

		}.bind(this))
		.catch(function(error) {
        	alert(error);
		}.bind(this));
    },

    postSubscribeObj : function(statusType, subscription) {

		// Send the information to the server with fetch API.
		// the type of the request, the name of the user subscribing,
		// and the push subscription endpoint + key the server needs
		// to send push messages

		var push_url = this.webPushDataElement.dataset.url;
		var browser = navigator.userAgent.match(/(firefox|msie|chrome|safari|trident)/ig)[0].toLowerCase();
		var data = {
			'status_type'	: statusType,
			'subscription'	: subscription.toJSON(),
			'browser'		: browser,
			'group'			: this.webPushDataElement.dataset.group
		};

		fetch(push_url, {
			'method'		: 'post',
			'headers'		: {'Content-Type': 'application/json'},
			'body'			: JSON.stringify(data),
			'credentials'	: 'include'
		})
		.then(function(response) {

			// Check the information is saved successfully into server
			if ((response.status == 201) && (statusType == 'subscribe')) {

				this.pushButton.dataset.active = 1;
				this.isPushEnabled = true;
				this.pushButton.querySelector('.tooltiptext').innerText = "Disable notification for this taco";

				this.sendConfirmationNotification();
			}

			// Check if the information is deleted from server
			if ((response.status == 202) && (statusType == 'unsubscribe')) {

				// Get the Subscription
				this.getSubscription(this.registration)
				.then(function(subscription) {

					// Remove the subscription
					subscription.unsubscribe()
						.then(function(successful) {

							this.pushButton.dataset.active = 0;
							this.isPushEnabled = false;
							this.pushButton.querySelector('.tooltiptext').innerText = "Get notified when the taco ends";

						}.bind(this));

				}.bind(this))
				.catch(function(error) {

					console.log('Error during unsubscribe from Push Notification', error);

				}.bind(this));
			}

		}.bind(this))
    },

    sendConfirmationNotification : function() {

    	new Notification('Subscribed!', {
            body: 'You will be notified when this 🌮 is about to end!',
			icon: '/static/images/timetaco-notification.png'
        });
    }
  };

  window.WebPush = WebPush;

})();

(function() {

	var TimeTacoOverlay = {

		isOpened : false,
		activeOverlay : null,

		initialize : function() {

			var openers = Array.from(document.querySelectorAll('[data-overlayopener]'));

			var self = this;

			for(var openerIndex in openers) {

				var opener = openers[openerIndex];
				var overlaySlug = opener.dataset.overlayopener;

				var overlay = document.querySelector('[data-overlay=' + overlaySlug + ']');

				overlay.querySelector('[data-closeoverlay]').addEventListener('click', () => {

					this.close();

				}, false);
				opener.addEventListener('click', function() {

					var overlaySlug = this.dataset.overlayopener;

					self.open(overlaySlug);

				}, false);
			}

			window.addEventListener('keydown', (event) => {

				if(event.keyCode != 27)
					return;

				this.close();

			}, false);

			if(this.shouldShowLoginOverlay()) {

				this.open('login');
			}
		},

		shouldShowLoginOverlay : function() {

			if(location.search.length == 0)
				return false;

			var args = location.search.split(/\?/)[1].split(/&/);

			for(argIndex in args) {

				var arg = args[argIndex];

				var components = arg.split(/=/);

				if(components[0] == 'next')
					return true;
			}

			return false;
		},

		open : function(overlaySlug) {

			if(this.isOpened)
				return;

			var overlay = document.querySelector('[data-overlay=' + overlaySlug + ']');

			if(typeof overlay == 'undefined')
				return;

			this.isOpened = true;

			this.activeOverlay = overlay;

			this.activeOverlay.style.display = 'block';

			var input = this.activeOverlay.querySelector('input');

			if(typeof input != 'undefined') {

				if(input) {

					input.addEventListener('focus', function(e) {

						this.setSelectionRange(0, this.value.length);

						var copied = false;

						try {

							copied = document.execCommand('copy');

						} catch(err) {}

						e.stopPropagation();

					}, false);
				}
			}
		},

		close : function() {

			if(!this.isOpened)
				return;

			this.isOpened = false;
			this.activeOverlay.style.display = '';
		}
	};

	window.TimeTacoOverlay = TimeTacoOverlay;

})();

(function() {

	var TimeTacoGenerator = {

	    acceptedTypes : [
	      'image/png',
	      'image/jpeg',
	      'image/jpg',
	      'image/gif'
	    ],

	    dragSupport: true,
	    isUploading: false,

	    form : null,
	    footer: null,
	    formElements : null,
	    tabbedElements : null,
	    yearElement: null,
	    yearElementTempName: "timetaco-year",
	    submitButton : null,
	    uploadProgress : null,
	    backgroundContainer : null,
	    imageUploaderLabel : null,
	    daytime : null,
	    section : null,
	    dropElement : null,
	    imageUploader: null,
	    removeBackground: null,
	    refreshBackground: null,

	    windowWidth: 0,
	    windowHeight: 0,

	    unsplashEnabled : true,
	    isLoadingFromUnsplash : false,
	    unsplashDownloadLocation : undefined,

	    hasBackground : false,

		initialize : function() {

			if(document.getElementById('section') == null)
				return;

			this.section = document.getElementById('section');
			this.footer = document.getElementById('footer');
			this.form = document.getElementById('form');
			this.daytime = document.getElementById('daytime');
			this.dayContainer = document.querySelector('#daytime .day');
			this.timeContainer = document.querySelector('#daytime .time');
			this.formElements = document.querySelectorAll('#form .day input');
			this.tabbedElements = document.querySelectorAll('#form .day input, #form .time input, #form input[type="text"]');
			this.yearElement = document.querySelector('input[name="year"]');
			this.submitButton = document.querySelector('input[type="submit"]')
			this.uploadProgress = document.getElementById('upload-progress');
			this.backgroundContainer = document.getElementById('background-container');
			this.imageUploaderLabel = document.getElementById('image-uploader-label');
			this.dropElement = document.getElementById('image-uploader-drop');
			this.imageUploader = document.getElementById('background');
			this.removeBackground = document.getElementById('remove-background');
			this.refreshBackground = document.getElementById('refresh-background');
			this.titleElement = document.querySelector('input[name="title"]');
			this.attributionElement = document.getElementById('attribution');

			if (this.refreshBackground != null) {
				this.refreshBackground.addEventListener('click', function() {

					if(this.isUploading)
						return;

					this.fetchUnsplashBackground();

				}.bind(this), false);
			}

			if (this.removeBackground != null) {
				this.removeBackground.addEventListener('click', function() {

					if(this.isUploading)
						return;

					this.refreshBackgroundWithURL();

				}.bind(this), false);
			}

			this.yearElement.name = this.yearElementTempName;

			if (document.querySelector('input[name="background_url"]') != null
					&& this.dragSupported()) {
				document.addEventListener('drop', this.onDocumentDrop.bind(this), false);
				document.addEventListener('dragover', this.onDocumentDragOver.bind(this), false);
			}

			// Detect if there is already a file (user has clicked 'back' on his browser)
			if (document.querySelector('input[name="background_url"]') != null
					&& document.querySelector('input[name="background_url"]').value != '') {
				this.refreshBackgroundWithURL(document.querySelector('input[name="background_url"]').value);
				this.validateForm();
			}

			if (this.imageUploader != null) {
				this.imageUploader.addEventListener('change', function(event) {

					document.querySelector('input[name="unsplash_author_name"]').value = '';
					document.querySelector('input[name="unsplash_author_url"]').value = '';
					this.attributionElement.innerHTML = '';

					if(event.target.files.length > 0)
						this.uploadFile(event.target.files[0]);

				}.bind(this), false);
			}

			// Fixes the sticky hover effects in iOS
			if (this.imageUploaderLabel != null) {
				this.imageUploaderLabel.addEventListener('click', function(event) {

				    setTimeout(function() {

				    	this.className = '';

				 	}.bind(this), 300);

				}.bind(this), false);
			}

			for(var index = 0; index < this.tabbedElements.length; index++) {

				var element = this.tabbedElements[index];

				element.addEventListener('focus', function() {

					if(this.value != '')
						return;

					var currentDate = new Date();

					if(this.name == 'timetaco-year' || this.name == 'year')
						this.value = currentDate.getFullYear()
					else if(this.name == 'month')
						this.value = (currentDate.getMonth() + 1);
					else if(this.name == 'day')
						this.value = currentDate.getDate();
					else if(this.name == 'hour')
						this.value = (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours();
					else if(this.name == 'minute')
						this.value = (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes();

				}, false);

				element.addEventListener('blur', this.validateForm.bind(this), false);
				element.addEventListener('change', this.validateForm.bind(this), false);
			}

			this.form.addEventListener('submit', function(event) {

				event.preventDefault();

				if(!this.isFormReady())
					return false;

				this.triggerUnsplashDownload(() => {

					this.submitForm();
				});

				return false;

			}.bind(this), false);

			this.detectExistingDate();

			this.validateForm();
		},

		detectExistingDate : function() {

			if(this.daytime.dataset.year == undefined)
				return;

			var month = this.daytime.dataset.month - 1;
			var day = this.daytime.dataset.day;
			var year = this.daytime.dataset.year;
			var hour = this.daytime.dataset.hour;
			var minute = this.daytime.dataset.minute;

			var existingDate = new Date(year, month, day, hour, minute);

			// The date is in UTC, add the local TZ
			var tzDifference = new Date().getTimezoneOffset();

			var offsetTime = new Date(existingDate.getTime() - tzDifference * 60 * 1000);

			existingDate = new Date(offsetTime);

			document.querySelector('input[name="timetaco-year"]').value = existingDate.getFullYear();
			document.querySelector('input[name="month"]').value = existingDate.getMonth() + 1;
			document.querySelector('input[name="day"]').value = existingDate.getDate();

			if(existingDate.getHours() != 0 || existingDate.getMinutes() != 0) {

				document.querySelector('input[name="hour"]').value = existingDate.getHours();
				document.querySelector('input[name="minute"]').value = existingDate.getMinutes();
			}
		},

		fetchUnsplashBackground : function() {

			if(this.isLoadingFromUnsplash)
				return;

			if(!this.unsplashEnabled)
				return;

			this.refreshBackground.dataset['animating'] = '1';
			this.unsplashDownloadLocation = undefined;

			var title = this.titleElement.value;
			var searchTerm = undefined;

		    var usRequest = new XMLHttpRequest();
		    usRequest.open('GET', '//api.unsplash.com/photos/random/?client_id=' + US_API_KEY + (searchTerm ? '&' + searchTerm : ''), true);
		    usRequest.onload = function() {

				this.refreshBackground.dataset['animating'] = '';
		    	this.isLoadingFromUnsplash = false;

		    	if(usRequest.status != 200) {

		    		if(parseInt(usRequest.getResponseHeader('X-Ratelimit-Remaining')) == 0) {

		    			this.unsplashEnabled = false;
		    			this.refreshBackground.style.display = 'none';
		    		}

		    		return;
		    	}

		        var response = JSON.parse(usRequest.response);

		        if(response.urls) {

		        	var imgSrc = response.urls.regular;

		        	if(response.user) {

						document.querySelector('input[name="unsplash_author_name"]').value = response.user.name;
						document.querySelector('input[name="unsplash_author_url"]').value = response.user.links.html;

						this.attributionElement.innerHTML = '<a href="' + response.user.links.html + '?utm_source=TimeTaco&utm_medium=referral&utm_campaign=api-credit" target="_blank" title="' + response.user.name + '">' + response.user.name +'</a> <span>/</span> <a href="https://unsplash.com?utm_source=TimeTaco&utm_medium=referral&utm_campaign=api-credit" title="Unsplash" target="_blank">Unsplash</a>'
					}

		        	this.loadBackgroundImageFromURL(imgSrc);
		        	this.unsplashDownloadLocation = response.links.download_location;
		        }

		    }.bind(this);
		    usRequest.onerror = () => {
				this.refreshBackground.dataset['animating'] = '';
		    	this.isLoadingFromUnsplash = false;
		    };
		    usRequest.send();
		},

		triggerUnsplashDownload : function(completion) {

			if (typeof this.unsplashDownloadLocation != 'undefined')  {

				var downloadRequest = new XMLHttpRequest();
				downloadRequest.open('GET', this.unsplashDownloadLocation + '?client_id=' + US_API_KEY, true);
				downloadRequest.onload = () => {

					this.unsplashDownloadLocation = undefined;

					if(completion)
						completion();
				}
				downloadRequest.send();
			}
			else if(completion)
				completion();
		},

		uploadingStatus : function(enabled) {

			if(enabled) {

				this.form.className = 'uploading';
				if (this.refreshBackground != null)
					this.refreshBackground.className = 'tooltip disabled';
				if (this.removeBackground != null)
					this.removeBackground.className = 'tooltip disabled';
				this.submitButton.enabled = false;
				this.isUploading = true;
				this.backgroundContainer.className = 'uploading';
				this.uploadProgress.style.opacity = '1';
				this.imageUploaderLabel.innerHTML = 'Uploading';

			}
			else {

				if (this.unsplashEnabled
						&& this.refreshBackground != null) {
					this.refreshBackground.className = 'tooltip';
					this.refreshBackground.style.display = 'block';
				}

				this.form.className = '';
				this.removeBackground.className = 'tooltip';
				this.submitButton.enabled = true;
				this.isUploading = false;
				this.backgroundContainer.className = '';
				this.uploadProgress.style.opacity = '0';
				this.uploadProgress.style.width = '0%';
				this.imageUploaderLabel.innerHTML = 'Background image';
			}

			this.validateForm();
		},

		uploadingStatusProgress : function(progress) {

			this.uploadProgress.style.width = progress + '%';
		},

		uploadFile: function(file) {

		    if(this.acceptedTypes.indexOf(file.type) == -1) {

		    	alert('Image type not supported! Accepted image types: JPG, PNG, GIF.')
		    	return;
		    }

		    if(file.size > 10485760) {

		    	alert('Image too large! Maximum file size: ' + Math.round(10485760 / 1000000) + ' Mb');
		    	return;
		    }

	        this.uploadingStatus(true);

			var formData = new FormData();
			formData.append('file', file);
			formData.append('upload_preset', 'oipiv7bd');

	        var xhr = new XMLHttpRequest();
	        xhr.open('POST', 'https://api.cloudinary.com/v1_1/timetaco/auto/upload', true);

	        var self = this;

			xhr.onreadystatechange = function(e) {

			  if (this.readyState == 4) {

				if(this.status == 200) {

					var json = JSON.parse(this.responseText);

		            document.querySelector('input[name="background_url"]').value = json.secure_url;

					self.refreshBackgroundWithURL(json.secure_url);
				}

			    self.uploadingStatus(false);
			  }
			};
			xhr.upload.onerror = function() {
				self.uploadingStatus(false);
			};
	        xhr.upload.onprogress = function(e) {

				if (e.lengthComputable)
					self.uploadingStatusProgress((e.loaded / e.total) * 100);
	        };
	        xhr.send(formData);
		},

		submitForm: function() {

			// Check if date is valid
			var year = this.yearElement.value;
			var month = document.querySelector('input[name="month"]').value - 1;
			var day = document.querySelector('input[name="day"]').value;
			var hour = document.querySelector('input[name="hour"]').value;
			var minute = document.querySelector('input[name="minute"]').value;
			var hours = 0;
			var minutes = 0;

			if(hour != undefined && hour != '')
				hours = hour;

			if(minute != undefined && minute != '')
				minutes = minute;

			var date = new Date(year, month, day, hours, minutes);

			if(date.getMonth() != month || date.getDate() != day || date.getFullYear() != year || date.getMinutes() != minutes || date.getHours() != hours) {

				this.timeContainer.dataset.warning = '1';
				this.dayContainer.dataset.warning = '1';

				setTimeout(function() {
					this.timeContainer.dataset.warning = '';
					this.dayContainer.dataset.warning = '';
				}.bind(this), 2000);

				return;
			}

			var nowDate = new Date();

			if(date <= nowDate) {

				if(date.getMonth() == nowDate.getMonth() && date.getDate() == nowDate.getDate() && date.getFullYear() == nowDate.getFullYear())
					this.timeContainer.dataset.warning = '1';
				else
					this.dayContainer.dataset.warning = '1';

				setTimeout(function() {
					this.timeContainer.dataset.warning = '';
					this.dayContainer.dataset.warning = '';
				}.bind(this), 2000);

				return;
			}

			document.querySelector('input[name="target"]').value = date.toISOString().substr(0, 19).replace('T', ' ');

			if(this.isUploading)
				return;

			this.yearElement.name = "year";

			this.form.submit();
		},

		loadBackgroundImageFromURL : function(url) {

			var xhr = new XMLHttpRequest();
			xhr.open('GET', url, true);
			xhr.responseType = 'blob';

			var self = this;

			xhr.onload = function(e) {
			  if (this.status == 200) {

			    var blob = new Blob([this.response], {type: 'image/png'});

			    self.uploadFile(blob);
			  }
			};

			xhr.send();
		},

		onDocumentDrop: function(event) {

	    event.stopPropagation();
	    event.preventDefault();

			document.querySelector('input[name="unsplash_author_name"]').value = '';
			document.querySelector('input[name="unsplash_author_url"]').value = '';
			this.attributionElement.innerHTML = '';

		    // Allow users to drag and drop image urls
			if(event.dataTransfer.getData("Url")) {

				var url = event.dataTransfer.getData("Url");

				this.loadBackgroundImageFromURL(url);
			}
			else {

				var files = event.dataTransfer.files;

				if(files.length > 0)
					this.uploadFile(files[0]);
			}
		},

 		onDocumentDragOver: function(event) {

		    event.stopPropagation();
		    event.preventDefault();

		    return false;
		},

		validateForm: function(event) {

			this.daytime.className = '';

			var isReady = this.isFormReady();

			var classes = [];

			if(isReady)
				classes.push('ready')

			if(!this.isUploading)
				classes.push('pulse');

			this.footer.className = classes.join(' ');
			this.submitButton.style.display = (isReady ? 'block' : 'none');
		},

		isFormReady: function() {

			var isReady = true;

			for(var index = 0; index < this.formElements.length; index++) {

				var element = this.formElements[index];

				if(element.value == '') {

					isReady = false;
					break;
				}
			}

			return isReady;
		},

		checkImageURL : function(url) {
		    return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
		},

		dragSupported : function() {

			if (!this.dragSupport)
				return false;

			if (this.isMobileOrTablet())
				return false;

			return ('draggable' in document.createElement('span'));
		},

		isMobileOrTablet : function() {
		  var check = false;
		  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
		  return check;
		},

		refreshBackgroundWithURL : function(url) {

			if(url) {

				document.body.style.backgroundImage = 'url(\'' + url + '\')';

				this.hasBackground = true;

				document.body.className = 'enabled';

				this.removeBackground.style.display = 'block';
			}
			else {

				document.querySelector('input[name="background_url"]').value = '';
				document.querySelector('input[name="unsplash_author_name"]').value = '';
				document.querySelector('input[name="unsplash_author_url"]').value = '';
				this.attributionElement.innerHTML = '';

				this.imageUploader.value = '';

				document.body.style.backgroundImage = '';

				this.hasBackground = false;

				document.body.className = '';

				this.removeBackground.style.display = 'none';
			}

			ColorExtractor.parseBackgroundImage();
		}
	};

	window.TimeTacoGenerator = TimeTacoGenerator;
})();

(function() {

	var TimeTacoMeter = {

		paused: false,
		countdown: null,
		countdownParent: null,
		date: null,
		maxRenderedElements: 4,
		editField : null,
		closeButton : null,
		editContainer : null,
		displayStatus : 0, // 0 for countdown, 1 for date

		initialize : function() {

			if(document.getElementById('countdown') == null)
				return;

			this.countdownParent = document.getElementById('countdown-parent');
			this.countdown = document.getElementById('countdown');
			this.editField = document.getElementById('edit-field');
			this.closeButton = document.getElementById('close');
			this.editContainer = document.getElementById('edit');

			var month = this.countdown.dataset.month - 1;
			var day = this.countdown.dataset.day;
			var year = this.countdown.dataset.year;
			var hour = this.countdown.dataset.hour;
			var minute = this.countdown.dataset.minute;

			this.date = new Date(year, month, day, hour, minute);

			// The date is in UTC, add the local TZ
			var tzDifference = new Date().getTimezoneOffset();

			var offsetTime = new Date(this.date.getTime() - tzDifference * 60 * 1000);

			this.date = new Date(offsetTime);

			if(this.closeButton) {

				this.closeButton.addEventListener('click', function() {

					this.editContainer.parentElement.removeChild(this.editContainer);

				}.bind(this), false);
			}

			if(this.editField) {

				this.editField.addEventListener('click', function() {

		            this.focus();
		            this.setSelectionRange(0, this.value.length);

		            var copied = false;
		            try {
		                copied = document.execCommand('copy');
		            } catch (err) {}

				}, false);
			}

			if(this.countdownParent.dataset.toggle === "true") {

				this.countdownParent.addEventListener('click', function() {

					this.displayStatus = (this.displayStatus == 0 ? 1 : 0);
					this.countdown.className = (this.displayStatus == 1 ? 'date' : '');
					this.render();

				 }.bind(this), false);
			}

			this.render();
		},

		render: function() {

			if(this.displayStatus == 1) {

				var text = '<p>';

				text += '<span>'
				text += '<strong>';
				text += this.date.getFullYear() + '/';
				text += (this.date.getMonth() + 1) + '/';
				text += this.date.getDate() + ' ';
				text += this.pad(this.date.getHours(), 2) + ':';
				text += this.pad(this.date.getMinutes(), 2);
				text += '</strong>';
				text += '<em>Scheduled Date</em>';
				text += '</span>';
				text += '</p>';

				this.countdown.dataset['hash'] = '';
				this.countdown.innerHTML = text;

				return;
			}

			var components = this.extractComponents(this.date);
			let hash 	   = this.hashFromComponents(components);

			if(typeof this.countdown.dataset['hash'] != undefined
				&& this.countdown.dataset['hash'] == hash)
			{
				if(components.length > 0 && !this.paused)
					requestAnimationFrame(this.render.bind(this));

				return;
			}

			var text = '';

			if(components.length > 0) {

				text += '<p>';

				components.forEach((component) => {

					Object.entries(component).forEach(([key, value]) => {

						text += '<span>';
						text += '<strong>' + this.pad(value, 2) + '</strong>';
						text += '<em>';

						if(key == 'y')
							text += (value == 1 ? 'year' : 'years');
						else if(key == 'mo')
							text += (value == 1 ? 'month' : 'months');
						else if(key == 'd')
							text += (value == 1 ? 'day' : 'days');
						else if(key == 'h')
							text += (value == 1 ? 'hour' : 'hours');
						else if(key == 'mi')
							text += (value == 1 ? 'minute' : 'minutes');
						else if(key == 's')
							text += (value == 1 ? 'second' : 'seconds');

						text += '</em>';
						text += '</span>';				
					});
				});

				text += '</p>';
			}
			else {
				text += '<p class="happened">Already happened!</p>';
				this.countdown.parentNode.className = "countdown-container happened";
			}

			this.countdown.dataset['hash'] = hash;
			this.countdown.innerHTML = text;

			if(components.length > 0 && !this.paused)
				requestAnimationFrame(this.render.bind(this));
		},

		hashFromComponents : function(components)
		{
			var hash = '';

			components.forEach((component) => {

				Object.entries(component).forEach(([key, value]) => {

					hash += key + value;						
				});
			});

			return hash;
		},

		extractComponents : function(date)
		{
			let cdObject	= countdown(date);

			if(cdObject.value >= 0)
				return [];

			let components = [];
			var elementsRendered = 0;

			if(cdObject.years > 0) {

				components.push({'y' : cdObject.years});
				elementsRendered++;
			}

			if(cdObject.months > 0) {

				components.push({'mo' : cdObject.months});
				elementsRendered++;
			}

			components.push({'d' : cdObject.days});
			elementsRendered++;

			components.push({'h' : cdObject.hours});
			elementsRendered++;

			if(elementsRendered < this.maxRenderedElements) {

				components.push({'mi' : cdObject.minutes});
				elementsRendered++;
			}

			if(elementsRendered < this.maxRenderedElements) {

				components.push({'s' : cdObject.seconds});
			}

			return components;
		},

		pause : function() {

			if(this.paused)
				return;

			this.paused = true;
		},

		resume : function() {

			if(!this.paused)
				return;

			this.paused = false;

			this.render();
		},

		pad: function(num, size) {

		    var s = num+"";
		    while (s.length < size) s = "0" + s;
		    return s;
		}
	};

	window.TimeTacoMeter = TimeTacoMeter;
})();

window.addEventListener('DOMContentLoaded', function() {

	ColorExtractor.initialize();
	TimeTacoOverlay.initialize();
	TimeTacoMeter.initialize();
	TimeTacoGenerator.initialize();
	WebPush.initialize();
	Tooltips.initialize();

	AccountDetails.initialize();
	ConfirmAction.initialize();

}, false);
