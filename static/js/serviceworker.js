self.addEventListener('push', function(event) {

  var payload = event.data ? event.data.text() : {"head": "No Content", "Body": "No Content"};
  var data = null;

  try{
    data  = JSON.parse(payload);
  }catch(e){

    data = {
      'head': 'TimeTaco',
      'body': payload,
      'id': ''
    };
  }

  if(data == null)
    return;

  var head = data.head,
      body = data.body;

  // Keep the service worker alive until the notification is created.
  event.waitUntil(

    self.registration.showNotification(head, {
      icon: '../images/timetaco-notification.png',
      body: body,
      actions: [
        {action: 'visit', title: '🕐 Take me there!'},
        {action: 'close', title: '🌮 Nah, I\'d rather have a taco'}
      ],
      data: {
        'id': data.id
      }
    })
  );
});

self.addEventListener('notificationclick', function(event) {

  var tacoId = event.notification.data.id;

  event.notification.close();

  if (event.action !== 'close')
    clients.openWindow("/" + tacoId);

}, false);